# Contributing to AtoM Installer

* Update the Contributors section of the [README.md](https://gitlab.com/guillearch/atom-installer/blob/master/README.md) file with your full name or nickname and an URL to your GitLab repo.
* Don't write a Pull Request until an issue has been opened and assigned to you. Feel free to open a new issue if you want to work on some feature, fix a bug...
* Don't write a Pull Request for closed issues.
* Before submitting a Pull Request, check your spelling and grammar and make sure that you inclued a reference to the issue that you are working on.

Thank you for contributing to open source projects. You're awesome! :smiley:
