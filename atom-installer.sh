#!/bin/bash
# AtoM Installer for GNU/Linux

usage="AtoM installer for GNU/Linux

Usage: $(basename "$0") [-h]  [-v n]

Where:
	-h	Display this help text
	-v	Set the version to install (2-4, 2-5...). Default: latest stable version"

version=2-8

while getopts ':hv:' option; do
	case "$option" in
		h)
			echo "$usage"
			exit;;
		v)
			version=$OPTARG;;
		:)
			printf "\033[1;31mmissing argument for -%s\033[0m\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1;;
		\?)
			printf "\033[1;31millegal option: -%s\033[0m\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1;;
	esac
done

shift $((OPTIND - 1))

case $version in
	2-8)
		printf "Installing AtoM 2.8...\n"
		chmod +x ./versions/2-8/atom-2-8.sh
		/bin/bash ./versions/2-8/atom-2-8.sh;;
	2-7)
		printf "Installing AtoM 2.7...\n"
		chmod +x ./versions/2-7/atom-2-7.sh
		/bin/bash ./versions/2-7/atom-2-7.sh;;
	2-6)
		printf "Installing AtoM 2.6...\n"
		chmod +x ./versions/2-6/atom-2-6.sh
		/bin/bash ./versions/2-6/atom-2-6.sh;;
	2-5)
		printf "Installing AtoM 2.5...\n"
		chmod +x ./versions/2-5/atom-2-5.sh
		/bin/bash ./versions/2-5/atom-2-5.sh;;
	2-4)
		printf "Installing AtoM 2.4...\n"
		chmod +x ./versions/2-4/atom-2-4.sh
		/bin/bash ./versions/2-4/atom-2-4.sh;;
	*)
		echo "\033[1;31mPlease enter a valid version number (2-4, 2-5...). Lowest version supported: 2.4\033[0m\n";;
esac
