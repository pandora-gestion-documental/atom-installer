#!/bin/bash
# AtoM 2.4 Installer

. /etc/os-release
version="$NAME $VERSION_ID"
case $version in
	"Ubuntu 16.04")
		chmod +x ./versions/2-4/atom-2-4-ubuntu-16-04.sh
		sudo sh versions/2-4/atom-2-4-ubuntu-16-04.sh;;
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.4.\033[0m\n"
		exit
esac
