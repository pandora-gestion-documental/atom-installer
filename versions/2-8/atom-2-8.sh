#!/bin/bash
# AtoM 2.8 Installer

. /etc/os-release
version="$NAME $VERSION_ID"
case $version in
	"Ubuntu 20.04")
		chmod +x ./versions/2-8/atom-2-8-ubuntu-20-04.sh
		sudo sh versions/2-8/atom-2-8-ubuntu-20-04.sh;;	
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.8.\033[0m\n"
		exit
esac
