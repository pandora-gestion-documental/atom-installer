#!/bin/bash
# AtoM 2.8.2 Ubuntu 20.04 LTS Installer

# Updating and upgrading software
apt update -y
apt upgrade -y

# Install MySQL
apt install -y mysql-server
echo "Choose the password for the database 'root' user:"
read rootpassword
mysql -h localhost -u root -p -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by '$rootpassword';"
echo "[mysqld]
sql_mode=ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
optimizer_switch='block_nested_loop=off'
" >> /etc/mysql/conf.d/mysqld.cnf
systemctl restart mysql

# Install Elasticsearch
sudo apt install -y openjdk-11-jre-headless apt-transport-https software-properties-common
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-5.x.list
apt update -y
apt install -y elasticsearch
sed -i 's/#START_DAEMON=true/START_DAEMON=true/' /etc/default/elasticsearch
systemctl enable elasticsearch
systemctl start elasticsearch

# Install Ngingx
apt install -y nginx
touch /etc/nginx/sites-available/atom
ln -sf /etc/nginx/sites-available/atom /etc/nginx/sites-enabled/atom
rm /etc/nginx/sites-enabled/default
echo "upstream atom {
  server unix:/run/php7.4-fpm.atom.sock;
}

server {

  listen 80;
  root /usr/share/nginx/atom;

  # http://wiki.nginx.org/HttpCoreModule#server_name
  # _ means catch any, but it's better if you replace this with your server
  # name, e.g. archives.foobar.com
  server_name _;

  client_max_body_size 72M;

  # http://wiki.nginx.org/HttpCoreModule#try_files
  location / {
    try_files \$uri /index.php?\$args;
  }

  location ~ /\. {
    deny all;
    return 404;
  }

  location ~* (\.yml|\.ini|\.tmpl)\$ {
    deny all;
    return 404;
  }

  location ~* /(?:uploads|files)/.*\.php\$ {
    deny all;
    return 404;
  }

  location ~* /uploads/r/(.*)/conf/ {

  }

  location ~* ^/uploads/r/(.*)\$ {
    include /etc/nginx/fastcgi_params;
    set \$index /index.php;
    fastcgi_param SCRIPT_FILENAME \$document_root\$index;
    fastcgi_param SCRIPT_NAME \$index;
    fastcgi_pass atom;
  }

  location ~ ^/private/(.*)\$ {
    internal;
    alias /usr/share/nginx/atom/\$1;
  }

  location ~ ^/(index|qubit_dev)\.php(/|\$) {
    include /etc/nginx/fastcgi_params;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    fastcgi_split_path_info ^(.+\.php)(/.*)\$;
    fastcgi_pass atom;
  }

  location ~* \.php\$ {
    deny all;
    return 404;
  }

}" >> /etc/nginx/sites-available/atom
systemctl enable nginx
systemctl reload nginx

# Install PHP
apt install -y php-common php7.4-common php7.4-cli php7.4-curl php7.4-json php7.4-ldap php7.4-mysql php7.4-opcache php7.4-readline php7.4-xml php7.4-fpm php7.4-mbstring php7.4-xsl php7.4-zip php-apcu php-apcu-bc
apt-get install -y memcached
touch /etc/php/7.4/fpm/pool.d/atom.conf
echo "[atom]

; The user running the application
user = www-data
group = www-data

; Use UNIX sockets if Nginx and PHP-FPM are running in the same machine
listen = /run/php7.4-fpm.atom.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0600

; The following directives should be tweaked based in your hardware resources
pm = dynamic
pm.max_children = 30
pm.start_servers = 10
pm.min_spare_servers = 10
pm.max_spare_servers = 10
pm.max_requests = 200

chdir = /

; Some defaults for your PHP production environment
; A full list here: http://www.php.net/manual/en/ini.list.php
php_admin_value[expose_php] = off
php_admin_value[allow_url_fopen] = on
php_admin_value[memory_limit] = 512M
php_admin_value[max_execution_time] = 120
php_admin_value[post_max_size] = 72M
php_admin_value[upload_max_filesize] = 64M
php_admin_value[max_file_uploads] = 10
php_admin_value[cgi.fix_pathinfo] = 0
php_admin_value[display_errors] = off
php_admin_value[display_startup_errors] = off
php_admin_value[html_errors] = off
php_admin_value[session.use_only_cookies] = 0

; APC
php_admin_value[apc.enabled] = 1
php_admin_value[apc.shm_size] = 64M
php_admin_value[apc.num_files_hint] = 5000
php_admin_value[apc.stat] = 0

; Zend OPcache
php_admin_value[opcache.enable] = 1
php_admin_value[opcache.memory_consumption] = 192
php_admin_value[opcache.interned_strings_buffer] = 16
php_admin_value[opcache.max_accelerated_files] = 4000
php_admin_value[opcache.validate_timestamps] = 0
php_admin_value[opcache.fast_shutdown] = 1

; This is a good place to define some environment variables, e.g. use
; ATOM_DEBUG_IP to define a list of IP addresses with full access to the
; debug frontend or ATOM_READ_ONLY if you want AtoM to prevent
; authenticated users
env[ATOM_DEBUG_IP] = \"10.10.10.10,127.0.0.1\"
env[ATOM_READ_ONLY] = \"off\"" >> /etc/php/7.4/fpm/pool.d/atom.conf
systemctl enable php7.4-fpm
systemctl start php7.4-fpm

# Install Gearman Job Server
apt install -y gearman-job-server
# https://groups.google.com/g/ica-atom-users/c/Us-YMx2elUU/m/Wa1Lpe_nBQAJ
perl -i -0pe 's/PARAMS="--listen=localhost/PARAMS="--listen=127.0.0.1/' /etc/default/gearman-job-server


# Install other packages
apt install -y --no-install-recommends fop libsaxon-java
sudo update-java-alternatives -s java-1.11.0-openjdk-amd64
apt install -y imagemagick ghostscript poppler-utils ffmpeg

# Download AtoM 2.7.2
wget https://storage.accesstomemory.org/releases/atom-2.8.2.tar.gz
mkdir /usr/share/nginx/atom
tar xzf atom-2.8.2.tar.gz -C /usr/share/nginx/atom --strip 1

# Update filesystem permissions
chown -R www-data:www-data /usr/share/nginx/atom

# Create the database
echo "Choose the password for the database 'atom' user:"
read password
mysql -h localhost -u root -p -e "CREATE DATABASE atom CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;"
mysql -h localhost -u root -p -e "CREATE USER 'atom'@'localhost' IDENTIFIED BY '$password';"
mysql -h localhost -u root -p -e "GRANT ALL PRIVILEGES ON atom.* TO 'atom'@'localhost';"
touch /etc/mysql/conf.d/mysqld.cnf
systemctl restart mysql
systemctl restart php7.4-fpm
systemctl restart elasticsearch

# Enable PDF processing by ImageMagick
perl -i -0pe 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<!-- <policy domain="coder" rights="none" pattern="PDF" \/> -->/' /etc/ImageMagick-6/policy.xml

# Promtp the user to run the web installer
cd /usr/share/nginx/atom/
php symfony tools:install
systemctl restart php7.4-fpm.service
chown -R www-data:www-data /usr/share/nginx/atom