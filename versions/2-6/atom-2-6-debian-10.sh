#!/bin/bash
# AtoM 2.6 Debian 10 Installer

# Updating and upgrading software
apt update -y
apt upgrade -y

# Install GnuPG
apt install -y gnupg

# Install MySQL
wget https://dev.mysql.com/get/mysql-apt-config_0.8.16-1_all.deb
dpkg -i mysql-apt-config_0.8.16-1_all.deb
apt update -y
apt install -y mysql-server
mysql_secure_installation
echo "[mysqld]
sql_mode=ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION
optimizer_switch='block_nested_loop=off'
" >> /etc/mysql/conf.d/mysqld.cnf
systemctl restart mysql

# Install Elasticsearch
apt-get install -y apt-transport-https lsb-release ca-certificates software-properties-common
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/
apt update -y
apt install -y adoptopenjdk-8-hotspot
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | apt-key add -
echo "deb https://artifacts.elastic.co/packages/5.x/apt stable main" | tee -a /etc/apt/sources.list.d/elastic-5.x.list
apt-get update -y && apt-get install -y elasticsearch
systemctl enable elasticsearch
systemctl start elasticsearch

# Install Ngingx
apt install -y nginx
touch /etc/nginx/sites-available/atom
ln -sf /etc/nginx/sites-available/atom /etc/nginx/sites-enabled/atom
rm /etc/nginx/sites-enabled/default
echo "upstream atom {
  server unix:/run/php7.2-fpm.atom.sock;
}

server {

  listen 80;
  root /usr/share/nginx/atom;

  # http://wiki.nginx.org/HttpCoreModule#server_name
  # _ means catch any, but it's better if you replace this with your server
  # name, e.g. archives.foobar.com
  server_name _;

  client_max_body_size 72M;

  # http://wiki.nginx.org/HttpCoreModule#try_files
  location / {
    try_files \$uri /index.php?\$args;
  }

  location ~ /\. {
    deny all;
    return 404;
  }

  location ~* (\.yml|\.ini|\.tmpl)\$ {
    deny all;
    return 404;
  }

  location ~* /(?:uploads|files)/.*\.php\$ {
    deny all;
    return 404;
  }

  location ~* /uploads/r/(.*)/conf/ {

  }

  location ~* ^/uploads/r/(.*)\$ {
    include /etc/nginx/fastcgi_params;
    set \$index /index.php;
    fastcgi_param SCRIPT_FILENAME \$document_root\$index;
    fastcgi_param SCRIPT_NAME \$index;
    fastcgi_pass atom;
  }

  location ~ ^/private/(.*)\$ {
    internal;
    alias /usr/share/nginx/atom/\$1;
  }

  location ~ ^/(index|qubit_dev)\.php(/|\$) {
    include /etc/nginx/fastcgi_params;
    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    fastcgi_split_path_info ^(.+\.php)(/.*)\$;
    fastcgi_pass atom;
  }

  location ~* \.php\$ {
    deny all;
    return 404;
  }

}" >> /etc/nginx/sites-available/atom
systemctl enable nginx
systemctl reload nginx

# Install PHP
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
apt update -y && apt install -y php7.2-cli php7.2-curl php7.2-json php7.2-ldap php7.2-mysql php7.2-opcache php7.2-readline php7.2-xml php7.2-fpm php7.2-mbstring php7.2-xsl php7.2-zip php-apcu
apt-get install -y memcached
touch /etc/php/7.2/fpm/pool.d/atom.conf
echo "[atom]

; The user running the application
user = www-data
group = www-data

; Use UNIX sockets if Nginx and PHP-FPM are running in the same machine
listen = /run/php7.2-fpm.atom.sock
listen.owner = www-data
listen.group = www-data
listen.mode = 0600

; The following directives should be tweaked based in your hardware resources
pm = dynamic
pm.max_children = 30
pm.start_servers = 10
pm.min_spare_servers = 10
pm.max_spare_servers = 10
pm.max_requests = 200

chdir = /

; Some defaults for your PHP production environment
; A full list here: http://www.php.net/manual/en/ini.list.php
php_admin_value[expose_php] = off
php_admin_value[allow_url_fopen] = on
php_admin_value[memory_limit] = 512M
php_admin_value[max_execution_time] = 120
php_admin_value[post_max_size] = 72M
php_admin_value[upload_max_filesize] = 64M
php_admin_value[max_file_uploads] = 10
php_admin_value[cgi.fix_pathinfo] = 0
php_admin_value[display_errors] = off
php_admin_value[display_startup_errors] = off
php_admin_value[html_errors] = off
php_admin_value[session.use_only_cookies] = 0

; APC
php_admin_value[apc.enabled] = 1
php_admin_value[apc.shm_size] = 64M
php_admin_value[apc.num_files_hint] = 5000
php_admin_value[apc.stat] = 0

; Zend OPcache
php_admin_value[opcache.enable] = 1
php_admin_value[opcache.memory_consumption] = 192
php_admin_value[opcache.interned_strings_buffer] = 16
php_admin_value[opcache.max_accelerated_files] = 4000
php_admin_value[opcache.validate_timestamps] = 0
php_admin_value[opcache.fast_shutdown] = 1

; This is a good place to define some environment variables, e.g. use
; ATOM_DEBUG_IP to define a list of IP addresses with full access to the
; debug frontend or ATOM_READ_ONLY if you want AtoM to prevent
; authenticated users
env[ATOM_DEBUG_IP] = \"10.10.10.10,127.0.0.1\"
env[ATOM_READ_ONLY] = \"off\"" >> /etc/php/7.2/fpm/pool.d/atom.conf
systemctl enable php7.2-fpm
systemctl start php7.2-fpm

# Install Gearman Job Server
apt install -y gearman-job-server

# Install other packages
apt install -y --no-install-recommends fop libsaxon-java
apt install -y imagemagick ghostscript poppler-utils ffmpeg

# Download AtoM 2.6
wget https://storage.accesstomemory.org/releases/atom-2.6.4.tar.gz
mkdir /usr/share/nginx/atom
tar xzf atom-2.6.4.tar.gz -C /usr/share/nginx/atom --strip 1

# Update filesystem permissions
chown -R www-data:www-data /usr/share/nginx/atom

# Create the database
echo "Choose the password for the database 'atom' user:"
read password
mysql -h localhost -u root -p -e "CREATE DATABASE atom CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;"
mysql -h localhost -u root -p -e "CREATE USER 'atom'@'localhost' IDENTIFIED BY '$password';"
mysql -h localhost -u root -p -e "GRANT ALL PRIVILEGES ON atom.* TO 'atom'@'localhost';"
touch /etc/mysql/conf.d/mysqld.cnf
systemctl restart mysql
systemctl restart php7.2-fpm
systemctl restart elasticsearch

# Enable PDF processing by ImageMagick
perl -i -0pe 's/<policy domain="coder" rights="none" pattern="PDF" \/>/<!-- <policy domain="coder" rights="none" pattern="PDF" \/> -->/' /etc/ImageMagick-6/policy.xml

# Promtp the user to run the web installer
printf "\033[1;32mWe are almost done!\nOpen your browser and connect to the host (\"localhost\" or the IP address of the remote machine) in order to complete the installation.\033[0m\n"
